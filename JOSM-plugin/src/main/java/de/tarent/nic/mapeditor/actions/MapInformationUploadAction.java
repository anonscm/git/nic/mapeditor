package de.tarent.nic.mapeditor.actions;

import de.tarent.nic.mapserver.MapServerClient;
import de.tarent.nic.mapserver.response.MapDataResponse;
import org.openstreetmap.josm.Main;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The MapInformationUploadAction uploads an .osm file to the map-server.
 */
public class MapInformationUploadAction extends MapServerAction {

    private static final Logger LOGGER = Logger.getLogger(MapInformationUploadAction.class.getName());
    private MapServerClient client;

    /**
     * Default constructor.
     */
    public MapInformationUploadAction() {
        super("Upload to nic...", "Upload map information to nic");
    }

    /**
     * The action performed when the user wants to upload the map information.
     *
     * @param event we don't use this event, it's just part of the interface
     */
    public void actionPerformed(final ActionEvent event) {
        if (isConnectedToMapServer()) {
            final JFileChooser chooser = getMapInformationChooser();
            try {
                final int returnVal = chooser.showOpenDialog(Main.parent);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    handleMapInformationUpload(chooser);
                }
            } catch (final IOException e) {
                JOptionPane.showMessageDialog(Main.parent, new JTextField("Map information could not be opened."));
                LOGGER.log(Level.SEVERE, e.toString());
            }
        }
    }

    /**
     * Handles the entire map information upload process.
     *
     * @param chooser the {@link JFileChooser}
     * @throws IOException when the map information upload failed
     */
	private void handleMapInformationUpload(final JFileChooser chooser) throws IOException {
		final MapDataResponse response = uploadMapInformationFile(chooser);
		JOptionPane.showMessageDialog(Main.parent, response.getStatusMessage());
	}
	
	/**
	 * Uploads the map information with the map server {@link #client}.
     *
	 * @param chooser the {@link JFileChooser}
	 * @return the {@link MapDataResponse}
	 * @throws IOException when map information upload failed
	 */
	private MapDataResponse uploadMapInformationFile(final JFileChooser chooser) throws IOException {
		final File selectedFile = chooser.getSelectedFile();
		final String fileName = selectedFile.getName();
		final File absoluteFile = selectedFile.getAbsoluteFile();
		final MapDataResponse response = client.uploadMapData("JOSMTest", fileName, absoluteFile);
		return response;
	}

	/**
	 * Gets the map information {@link JFileChooser}.
     *
	 * @return the {@link JFileChooser}
	 */
	private JFileChooser getMapInformationChooser() {
		final JFileChooser chooser = new JFileChooser();
		final FileNameExtensionFilter filter = new FileNameExtensionFilter("Map Information", "osm");
		chooser.setFileFilter(filter);
		return chooser;
	}
}
