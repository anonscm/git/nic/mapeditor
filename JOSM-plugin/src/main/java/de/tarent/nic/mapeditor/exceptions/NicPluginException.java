package de.tarent.nic.mapeditor.exceptions;

/**
 * Simple nic plugin exception
 */
public class NicPluginException extends Exception {

    /**
     * Default constructor.
     */
    public NicPluginException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     */
    public NicPluginException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param cause the cause of the exception
     */
    public NicPluginException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     * @param cause the cause of the exception
     */
    public NicPluginException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
