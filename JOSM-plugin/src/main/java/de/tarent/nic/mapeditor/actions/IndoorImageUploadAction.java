package de.tarent.nic.mapeditor.actions;

import de.tarent.nic.mapeditor.exceptions.NicPluginException;
import de.tarent.nic.mapserver.response.MapImageResponse;
import org.apache.commons.io.FilenameUtils;
import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.data.coor.LatLon;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.gui.layer.Layer;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The IndoorImageUploadAction uploads a map-image to the map-server for tiling.
 */
public class IndoorImageUploadAction extends MapServerAction {

    private static final Logger LOGGER = Logger.getLogger(IndoorImageUploadAction.class.getName());
    private static final boolean DEBUG = false;

    private static final String REFERENCE_POINT_LEFT_UPPER = "leftUpper";
    private static final String REFERENCE_POINT_RIGHT_LOWER = "rightLower";

    /**
     * Default constructor.
     */
    public IndoorImageUploadAction() {
        super("Upload indoor image now", "Upload indoor image of your location");
    }

    /**
     * The action performed when the user wants to upload an image.
     *
     * @param event we don't use this event, it's just part of the interface
     */
    public void actionPerformed(final ActionEvent event) {
        if (isConnectedToMapServer()) {

            File imageFile = null;
            Map<String, LatLon> geoReferencePoints = null;
            try {
                imageFile = getImageFile();
                geoReferencePoints = getGeoReferencePointsMap();
            } catch (NicPluginException e) {
                showErrorMessage(e.getMessage());
                LOGGER.log(Level.SEVERE, e.toString());
            }

            handleUpload(imageFile, geoReferencePoints);
        }
    }

    private void handleUpload(File imageFile, Map<String, LatLon> geoReferencePoints) {
        if (imageFile != null && geoReferencePoints != null) {
            try {
                handleImageUpload(imageFile, geoReferencePoints);
            } catch (IOException e) {
                showErrorMessage("Bilddatei konnte nicht hochgeladen werden. " +
                        "Dabei ist folgender Fehler aufgetreten: \n" +
                        e.toString());
                LOGGER.log(Level.SEVERE, e.toString());
            }
        }
    }

    /**
     * TODO: This must be available for all classes!
     * Shows simple JOptionPane error message
     *
     * @param message error to show
     */
    private void showErrorMessage(String message) {
        JOptionPane.showMessageDialog(Main.parent, message, "Nic-Plugin Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Gets the image file for the active pic layer plugin
     *
     * @return the image file
     * @throws NicPluginException if pic layer is not an active layer or image information couldn't be get
     */
    private File getImageFile() throws NicPluginException {
        final Layer picLayer = Main.map.mapView.getActiveLayer();
        //We don't need the osm layer but PicLayer
        if (picLayer instanceof OsmDataLayer) {
            throw new NicPluginException("Bitte aktivieren Sie erst die PicLayer-Ebene.");
        }
        final File imageFile = new File(picLayer.getToolTipText());

        if (!imageFile.exists()) {
            throw new NicPluginException("Die Bilddatei-Information konnte von der PicLayer-Ebende " +
                    "nicht geladen werden, \n" +
                    "oder die PicLayer-Ebene wurde nicht aktiviert.");
        }
        return imageFile;
    }

    /**
     * Returns the osm layer to get the geo points later
     *
     * @return osm layer or null if none found
     */
    private OsmDataLayer getOsmLayer() {
        OsmDataLayer osmLayer = null;
        final List<Layer> layers = Main.map.mapView.getAllLayersAsList();
        if (layers != null && layers.size() > 0) {
            for (Layer layer : layers) {
                if (layer instanceof OsmDataLayer) {
                    osmLayer = (OsmDataLayer) layer;
                    break;
                }
            }
        }
        return osmLayer;
    }

    /**
     * Gets the two reference points as an LatLon object in puts it into a map with REFERENCE_POINT_LEFT_UPPER or
     * REFERENCE_POINT_RIGHT_LOWER keys.
     *
     * @return map with to latlon geo reference points
     * @throws NicPluginException if no or not exactly two points were found
     */
    private Map<String, LatLon> getGeoReferencePointsMap() throws NicPluginException {

        final Map<String, LatLon> resultMap = new HashMap<String, LatLon>();

        final OsmDataLayer osmLayer = getOsmLayer();
        final DataSet dataSet = osmLayer.data;

        if (dataSet == null || dataSet.getNodes() == null) {
            throw new NicPluginException("Bitte erstellen Sie erst die Referenzpunkte.");
        }

        final List<Node> nodes = new ArrayList<Node>(osmLayer.data.getNodes());

        if (nodes.isEmpty()) {
            throw new NicPluginException("Bitte erstellen Sie erst die Referenzpunkte.");
        }
        if (nodes.size() == 2) {
            fillResultMap(resultMap, nodes);
        } else {
            throw new NicPluginException("Bitte platzieren Sie genau 2 Referenzpunkte oben links und unten rechts.");
        }

        return resultMap;
    }

    private void fillResultMap(Map<String, LatLon> resultMap, List<Node> nodes) {
        final Node node1 = nodes.get(0);
        final Node node2 = nodes.get(1);

        if (node1.getCoor().lat() > node2.getCoor().lat()) {
            resultMap.put(REFERENCE_POINT_LEFT_UPPER, node1.getCoor());
            resultMap.put(REFERENCE_POINT_RIGHT_LOWER, node2.getCoor());
        } else {
            resultMap.put(REFERENCE_POINT_LEFT_UPPER, node2.getCoor());
            resultMap.put(REFERENCE_POINT_RIGHT_LOWER, node1.getCoor());
        }
    }


    /**
     * Delegates the image upload to the more complex handleImageUpload with single coordinates
     *
     * @param imageFile                indoor image
     * @param getGeoReferencePointsMap reference map with two latlong reference points
     * @throws IOException in case of image file read problems
     */
    private void handleImageUpload(File imageFile, Map<String, LatLon> getGeoReferencePointsMap) throws IOException {
        final LatLon leftUpper = getGeoReferencePointsMap.get(REFERENCE_POINT_LEFT_UPPER);
        final LatLon rightLower = getGeoReferencePointsMap.get(REFERENCE_POINT_RIGHT_LOWER);


        final float left = (float) leftUpper.lon();
        final float upper = (float) leftUpper.lat();
        final float right = (float) rightLower.lon();
        final float lower = (float) rightLower.lat();

        handleImageUpload(imageFile, left, upper, right, lower);
    }


    /**
     * Handles the whole process of image uploading
     *
     * @param imageFile indoor image
     * @param left      upper left longitude
     * @param upper     upper left latitude
     * @param right     lower right longitude
     * @param lower     lower right latitude
     * @throws IOException
     */
    private void handleImageUpload(final File imageFile, float left, float upper,
                                   float right, float lower) throws IOException {
        final MapImageResponse response = uploadImage(imageFile, left, upper, right, lower);
        if (DEBUG) {
            JOptionPane.showMessageDialog(Main.parent, "Image path: " + imageFile +
                    " left upper: " + left + " " + upper + " right lower: " + right + " " + lower);
            return;
        }
        //TODO: Static {zoom}/{x}/{-y}.png MUST be delivered by the SERVER or configured as PROPERTY
        final String fullImageUrl = response.getImageURL() + "{zoom}/{x}/{-y}.png";
        JOptionPane.showMessageDialog(Main.parent, new JTextField(fullImageUrl));
    }

    /**
     * Uploads the image with the map server {@link #client}.
     *
     * @param imageFile the
     * @return the {@link MapImageResponse}
     * @throws IOException when the image upload failed
     */
    private MapImageResponse uploadImage(final File imageFile, float left, float upper,
                                         float right, float lower) throws IOException {
        //TODO: Null check for imageFile is already done before. Do it here to?
        final String mapName = FilenameUtils.removeExtension(imageFile.getName());
        final MapImageResponse response = client.uploadMapImage(mapName, imageFile.getAbsoluteFile(),
                left, upper, right, lower);
        return response;
    }
}
