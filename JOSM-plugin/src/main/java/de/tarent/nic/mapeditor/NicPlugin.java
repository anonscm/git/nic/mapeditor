package de.tarent.nic.mapeditor;

import de.tarent.nic.mapeditor.actions.IndoorImageUploadAction;
import de.tarent.nic.mapeditor.actions.MapInformationUploadAction;
import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.gui.MainMenu;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;

/**
 * A NIC plugin for the JOSM interface. This will add some menu options to upload images and the extra information
 * required for the map to the server.
 */
public class NicPlugin extends Plugin {
	
	/**
	 * Constructor.
     *
	 * @param info the {@link PluginInformation}
	 */
    public NicPlugin(final PluginInformation info) {
        super(info);
        MainMenu.add(Main.main.menu.imageryMenu, new IndoorImageUploadAction());
        Main.main.menu.imageryMenu.addSeparator();

        MainMenu.addAfter(Main.main.menu.fileMenu, new MapInformationUploadAction(), false, Main.main.menu.upload);
    }
}
