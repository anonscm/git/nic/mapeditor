package de.tarent.nic.mapeditor.actions;

import de.tarent.nic.mapserver.MapServerClient;
import de.tarent.nic.mapserver.MapServerClientImpl;
import de.tarent.nic.mapserver.exception.NicException;
import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.tools.Shortcut;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.openstreetmap.josm.tools.I18n.tr;

/**
 * Base {@link MapServerAction}.
 */
public abstract class MapServerAction extends JosmAction {

    /**
     * The {@link Logger} to log the information output.
     */
    private static final Logger LOGGER = Logger.getLogger(IndoorImageUploadAction.class.getName());

    /**
     * {@link MapServerClient} to be used by the map server actions.
     */
    protected transient MapServerClient client;

    /**
     * Constructor.
     *
     * This constructor should be used when no shortcuts are given for the action.
     *
     * @param menuText the text to display in the menu
     * @param mouseOverText the text to display on mouse over
     */
    public MapServerAction(final String menuText, final String mouseOverText) {
        super(tr(menuText), "nic", tr(mouseOverText),
                Shortcut.registerShortcut(null, null, Shortcut.NONE, Shortcut.NONE), true);
        try {
            //TODO: This should be in the properties (we don't have any yet)!
            client = new MapServerClientImpl("http://nic-mapserver.lan.tarent.de:8080/mapserver");
        } catch (final NicException e) {
            client = null;
            LOGGER.log(Level.SEVERE, e.toString());
        }
    }

    /**
     * If the {@link #client} is null (not connected) then show an error message and return false. Else return true.
     *
     * @return whether or not the map server is connected
     */
    protected boolean isConnectedToMapServer() {
        if (client == null) {
            JOptionPane.showMessageDialog(Main.parent, new JTextField("Mapserver-configuration is invalid."));
            LOGGER.log(Level.WARNING, "Mapserver-configuration is invalid.");
            return false;
        }
        return true;
    }
}
